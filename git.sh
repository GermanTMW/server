#!/bin/bash

export LANG="C";

REPO="git@gitlab.com:GermanTMW"; #write perms
ROOTDIR=$(pwd);	#static root directory
REPODIRS=(		"client-data" "server-data" "tools" "hercules"		"evol-hercules"			"server"); #RepoDirs
REPODIRNAME=(	"client-data" "server-data" "tools" "server-code" 	"server-code/src/evol"	"."		);  #RepoNames

typeset -i COUNT;
COUNT=0;

help()
{
	echo "usage $0 <init> | <status> [gitdir/a/all] | <pull> <gitdir>"; # | <push> [gitdir]";
	exit;
}

init()
{
	if [[ !$2 ]]; then
		for R in ${REPODIRS[@]}; do
			if [[ -d $ROOTDIR/${REPODIRNAME[$COUNT]} ]]; then
				echo -e "\033[0;31m[ERROR]\033[0m: $ROOTDIR/${REPODIRNAME[$COUNT]} exists already!";
			else
				echo -ne "cloning \e[0;32m${REPODIRNAME[$COUNT]}\e[0m! plz wait..."
				git clone $REPO/$R.git $ROOTDIR/${REPODIRNAME[$COUNT]} > /dev/null 2>&1
				echo -e " ...finished cloning \e[0;32m${REPODIRNAME[$COUNT]}\e[0m!"
			fi
			COUNT=$COUNT+1;
		done
		if [[ ! -d  "$ROOTDIR/server-plugin/" ]]; then
    		ln -s server-code/src/evol server-plugin
		else
			echo -e "\e[0;31m[ERROR]\e[0m: 'server-code/src/evol -> server-plugin' link already exists!"
		fi
	fi	
	exit;
}

checkstatus()
{
	if [[ -d $ROOTDIR/$1 ]]; then
		cd $ROOTDIR/$1;
		LOG=$(git diff --stat --color=always);
		LOG2=$(git status|grep -E "Your branch is (behind|ahead)")$(git status|grep -E "have diverged");
		LOG3=$(git status -s -uno);
		if [[ -n "${LOG}${LOG2}${LOG3}" ]]; then
			if [[ -n ${LOG2} ]]; then
				echo -e "$1 \e[1;32m${LOG2}\e[0m";
				nP=$(git rev-list master --count)
				P=$(git rev-list origin --count)
				count=$(($nP-$P));
				GLOG=$(git log --pretty=oneline --abbrev-commit -n$count)
				IFS=$'\n'
				for line in $GLOG; do
					echo -e "\t$line";
				done
			fi
			if [[ -n ${LOG} ]]; then
				echo "$1 ${LOG}";
			elif [[ -n ${LOG3} ]]; then
				echo -e "$1 \e[1;33m${LOG3}\e[0m";
			fi
		else
			echo -e "$1 \e[0;32mNo changed made here... ( WorkingDir = SRepoDir )\e[0m";
		fi
		cd $ROOTDIR;
	else
		help;
	fi
	return;
}

status()
{
	if [[ ! $1 ]]; then
		help;
	else
		if [[ "$1" == "a" || "$1" == "all" || "$1" == * ]]; then
			for R in ${REPODIRNAME[@]}; do
				checkstatus $R;
			done
		else
			checkstatus $1;
		fi
	fi
}

pull()
{
	if [[ ! $1 ]]; then
		help;
	else
		if [[ -d $ROOTDIR/$1 ]]; then
			cd $ROOTDIR/$1;
			echo "trying to pull $1"

			if [[ "$1" == "server-code" || "$1" == "server-code/" ]]; then
			    LOG=$(git pull --no-commit);
			    LOG=$LOG $(git fetch origin);
			else
				LOG=$(git pull)
			fi

			if [[ "$LOG" == "Already up-to-date." ]]; then
					COL="\e[0;32m";
			fi
			echo -e "$COL$1 : $LOG \e[0m"
			cd $ROOTDIR;
		fi
	fi
}

case $1 in
	init)
		init;
	;;
	status)
		status $2;
	;;
	pull)
		pull $2;
	;;
#	push)
#		push $2;
#	;;
	*)
		help;
	;;
esac

